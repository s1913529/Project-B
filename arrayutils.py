"""
Array Utilities for LJ-Simulation
Author: P. Wang s1913529

A library that exploits numpy broadcasting and calculates pair separations 
without iterative loops.

"""


import numpy as np

def pbc(x1, box):
    """
    Applies periodic boundary condition

    :param x1: array of positional vectors of particles
    :param box: array of size of box
    :return np.mod(x1,box): a mirror of particles inside specified box
    """
    return np.mod(x1, box)

def mic(x1, box):
    """
    Applies minimum image convention adapted to accomodate large arrays

    :param x1: array of positional vectors of particles
    :param box: array of size of box
    :return: array of nearest neighbour to supplied particles
    """
    #box = np.tile(box, (x1.shape[0], x1.shape[0], 1))
    return np.mod(x1 + 0.5*box, box) - 0.5*box

def mic_norm(x1, box):
    """
    Applies minimum image convention

    :param x1: array of positional vectors of particles
    :param box: array of size of box
    :return: array of nearest neighbour to supplied particles
    """
    return np.mod(x1 + 0.5*box, box) - 0.5*box
    
def gen_coord_list(p3d_list):
    """
    Generates numpy array of coordinates given a list of particle
    :param p3d_list: list of p3d instances
    :return: numpy of coordinates of p3d instances
    """
    coord_list = []
    for particle in p3d_list:
        coord_list.append(particle.pos)
    return np.array(coord_list)

def gen_r_array(p3d_list, box):
    """
    Generates numpy array of separation vectors of a system of particles

    :param p3d_list: list of p3d instances
    :param box: numpy array specifing box dimensions
    :return: numpy array of separation vectors with MIC applied
    """
    pos_list = gen_coord_list(p3d_list)
    pos_row = pos_list.reshape((1, 3*len(p3d_list)))
    r_array = np.tile(pos_list, len(p3d_list)) - pos_row
    return mic(r_array.reshape((len(p3d_list),len(p3d_list),3)), box)

def mag_calc(r_array):
    """
    Generates numpy array of magnitudes of r_array
    :param r_array: numpy array of separation vectors
    :return mag_array: numpy array of sizes of vectors in r_array
    """
    mag_array = np.sqrt(np.add.reduce(r_array**2,2))
    return mag_array

def disp_calc_msd(r_array):
    disp_sq = np.add.reduce(r_array**2, 1)
    return np.sum(disp_sq) 