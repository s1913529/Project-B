"""
Computer Modelling: A Lennard-Jones simulation of N-particles 
with Verlet Time Integrator

This programme uses the Verlet integration
method to simulate the dynamics of particles interacting
with a Lennard-Jones potential in a fcc. 

author: P. Wang s1913529
"""

import sys
import numpy as np
from particle3D import Particle3D as p3d
import arrayutils
import mdutilities_20210131 as mdutils
import reportutils
import argparse
from datetime import datetime

def force_LJ(r_array, mag_array):
    """
    Compute the forces on a system of particles according to the Lennard-Jones potential.

    :param r_array: ndarray of all r vectors between each pair of particles in a system
    :param mag_array: magnitudes of all vectors in r_array
    :return force: an array with the same dimensions as r_array but containing all the forces
    """
    i = 0
    force = []
    for row, mag in zip(r_array, mag_array):
        mag = np.reshape(mag, (r_array.shape[0],1))
        F = 48*(np.power(mag, -14.*np.ones_like(mag)) - 0.5*np.power(mag, -8.*np.ones_like(mag)))*row
        F = np.nan_to_num(F, nan = 0, posinf = 0, neginf = 0)
        force.append(F)
    return np.array(force)


def pot_energy_LJ(mag_array):
    """
    Compute the potential energy of a system of particles according to the Lennard-Jones potential.

    :param mag_array: magnitudes of all vectors in r_array
    :return E_p_total: a single float of the total potential energy
    :return potential: an array with the same dimensions as mag_array but containing all the potential energies
    """
    potential = 4*(np.power(mag_array, -12.*np.ones_like(mag_array))- np.power(mag_array, -6.*np.ones_like(mag_array)))
    potential = np.nan_to_num(potential, nan=0, posinf=0, neginf=0)
    E_p_total = 0.5*np.sum(potential)
    return E_p_total, potential

def kinetic(p3d_list):
    """
    Computed the total kinetic energy of a system of particles.
    MSD defined as 1/N * sum_i(|r_i(t) - r_i(0)|)**2

    :param p3d_list: list of p3d instances
    :return p3d.sys_kinetic(p3d_list): returns the total Ek of the system
    """
    return p3d.sys_kinetic(p3d_list)
# Begin main code

def msd(p3d_list, coord_list_initial):
    """
    Computes the mean square displacement of particles.

    :param p3d_list: list of p3d instances
    :param coord_list_initial: array of initail positions for n particles in the system
    :return msd: the mean square displacement of all the particles
    """
    coord_list = arrayutils.gen_coord_list(p3d_list)
    msd = 1 / len(p3d_list)* np.sum(np.linalg.norm(coord_list - coord_list_initial, axis = 1)**2)
    return msd

def rdf(mag_array, rho, rdf_file_handle):
    """
    Computes the radial distribution function of the system.

    :param mag_array: array of magnitudes of all r-vectors
    :param rho: float, density specified by user for simulation
    :param rdf_file_handle: file handle for rdf data to be saved to
    :return:
    """
    hist, hist_edge = np.histogram(mag_array, bins=100, weights= 1/(rho*4*np.pi*mag_array**2))
    hist = np.insert(hist, 0, [0])
    hist = np.nan_to_num(hist, nan = 0, posinf=100, neginf=-100)
    np.savetxt(rdf_file_handle, np.vstack((hist_edge, hist)).T, delimiter=',', newline='\n')
    return


parser = argparse.ArgumentParser(description='Running a simulation with parameters')
parser.add_argument('input', help='Name of file with simulation parameters.')
parser.add_argument('run_name', help='The name of the simulation run (for data output).')
parser.add_argument('--report', action='store_true', help='Specify if a pdf report is to be generated.')

def main():
    StartTime = datetime.now()
    #Parse user input for configuration
    args = parser.parse_args()
    infile_name = args.input
    outfile_name = args.run_name
   
    # Open output files
    outfile = open(outfile_name + '.xyz', "w")
    # Open output file for energy calculations
    outfile_energy = open(outfile_name + '_energy.dat', 'w')
    # Open output file for MSD calculations
    outfile_msd = open(outfile_name + '_msd.dat', 'w')
    # Writes column headings for both files
    outfile_msd.write('#Time, MSD\n')
    outfile_energy.write('#Time, E_Err, MSD\n')
    # Initialise parameters 
    outfile_rdf = open(outfile_name + '_rdf.dat', 'w')
    outfile_rdf.write('#Radius, RDF(r) \n')
    time = 0
    m = 1
    x0 = v0 = np.zeros(3)
    # Parse simulation parameters from config file
    dt, numstep, number_of_particles, T, rho = np.loadtxt(infile_name, comments='#', unpack=True)
    dt, numstep, number_of_particles, T, rho = float(dt), int(numstep), int(number_of_particles), float(T), float(rho)
    params = {'time_step': dt, 'num_step': numstep, 'n': number_of_particles, 'temp' : T, 'rho' : rho}
    # Get initial force
    i = 0
    #Initialises particle list
    particle_list = []
    while i < number_of_particles:
        particle_list.append(p3d('particle', m, x0, v0))
        i += 1

    #Initialises box, positions, and velocities of particles

    box, full_lattice = mdutils.set_initial_positions(rho, particle_list)
    mdutils.set_initial_velocities(T, particle_list)
    #Computes pair separation vectors and magnitudes
    r_array = arrayutils.gen_r_array(particle_list, box)
    mag_array = arrayutils.mag_calc(r_array)
    #Computes forces and energies
    force = force_LJ(r_array, mag_array)
    E_p, pot_energy = pot_energy_LJ(mag_array)
    E_k = kinetic(particle_list)
    energy_tot_initial = E_p + E_k
    #Stores initial position for use in MSD calculations
    initial_pos = arrayutils.gen_coord_list(particle_list)
    #Initialises np array for RDF calculations
    rdf_mag = np.array([])

    #Begin main integration loop
    for i in range(numstep):
        #Writes initial MSD value
        outfile_msd.write(f'{time}, {msd(particle_list, initial_pos)}\n')
        #Update forces
        for particle, f in zip(particle_list, force):
            particle.update_pos_2nd(dt, np.sum(f, axis=0))
            particle.pos = arrayutils.pbc(particle.pos, box)
        
        #Recompute separations and forces
        r_array_new = arrayutils.gen_r_array(particle_list, box)
        mag_array_new = arrayutils.mag_calc(r_array_new)
        force_new = force_LJ(r_array_new, mag_array_new)
        #Update velocities
        for particle, f, f_new in zip(particle_list, force, force_new):
            particle.update_vel(dt, 0.5*np.sum(f, axis = 0) + 0.5*np.sum(f_new, axis = 0))
        #Calculate energy error and write to output
        energy_tot = kinetic(particle_list) + pot_energy_LJ(mag_array_new)[0]
        energy_err = (energy_tot - energy_tot_initial)/energy_tot_initial * 100
        outfile_energy.write(f'{time}, {energy_err}\n')
        #Store samples for RDF from time = 250 onwards and every 10 time steps.
        if i > 250 and i % 10 == 0:
            np.fill_diagonal(mag_array_new, np.nan)
            mag_values = mag_array_new[~np.isnan(mag_array_new)]
            mag_values_flatten = mag_values.flatten()
            rdf_mag = np.concatenate((rdf_mag, mag_values_flatten))
        # Re-define force value
        force = force_new
        # Increase time
        time += dt
        
        # Output particle information

        # Append information to data lists
        """
        time_list.append(time)
        pos_1_list.append(p1.pos[0])
        pos_2_list.append(p2.pos[0])
        energy_list.append(energy)
        """
        #Writes trajectory file
        num_of_particles = len(particle_list)
        outfile.write(f'{num_of_particles}\n')
        outfile.write(f'Point = {i + 1}\n')
        for i, particle in enumerate(particle_list):
            outfile.write(f's{i + 1} {particle.pos[0]} {particle.pos[1]} {particle.pos[2]}\n')
        
        
    # Post-simulation:
    # Close output file
    outfile.close()
    outfile_energy.close()
    outfile_msd.close()
    print('rdf mag length:', len(rdf_mag))
    rdf(rdf_mag, rho, outfile_rdf)
    outfile_rdf.close()
    executionTime = datetime.now() - StartTime

    # Generates pdf report, if called for
    if vars(args)['report']:
        reportutils.genfigs(outfile_name)
        reportutils.genreport(outfile_name, params, executionTime)
    else:
        pass   
# Execute main method, but only when directly invoked
if __name__ == "__main__":
    main()

