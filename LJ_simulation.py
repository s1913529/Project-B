"""
Computer Modelling: A Lennard-Jones simulation of N-particles 
with Verlet Time Integrator

This programme uses the Verlet integration
method to simulate the dynamics of particles interacting
with a Lennard-Jones potential in a fcc. 

author: P. Wang s1913529
"""

import sys
import numpy as np
from particle3D import Particle3D as p3d
import arrayutils
import mdutilities_20210131 as mdutils
import reportutils
from datetime import datetime
import PySimpleGUI as sg
import subprocess

def force_LJ(r_array, mag_array):
    """
    Compute the forces on a system of particles according to the Lennard-Jones potential.

    :param r_array: ndarray of all r vectors between each pair of particles in a system
    :param mag_array: magnitudes of all vectors in r_array
    :return force: an array with the same dimensions as r_array but containing all the forces
    """
    i = 0
    force = []
    for row, mag in zip(r_array, mag_array):
        mag = np.reshape(mag, (r_array.shape[0],1))
        F = 48*(np.power(mag, -14.*np.ones_like(mag)) - 0.5*np.power(mag, -8.*np.ones_like(mag)))*row
        F = np.nan_to_num(F, nan = 0, posinf = 0, neginf = 0)
        force.append(F)
    return np.array(force)


def pot_energy_LJ(mag_array):
    """
    Compute the potential energy of a system of particles according to the Lennard-Jones potential.

    :param mag_array: magnitudes of all vectors in r_array
    :return E_p_total: a single float of the total potential energy
    :return potential: an array with the same dimensions as mag_array but containing all the potential energies
    """
    potential = 4*(np.power(mag_array, -12.*np.ones_like(mag_array))- np.power(mag_array, -6.*np.ones_like(mag_array)))
    potential = np.nan_to_num(potential, nan=0, posinf=0, neginf=0)
    E_p_total = 0.5*np.sum(potential)
    return E_p_total, potential

def kinetic(p3d_list):
    """
    Computed the total kinetic energy of a system of particles.
    MSD defined as 1/N * sum_i(|r_i(t) - r_i(0)|)**2

    :param p3d_list: list of p3d instances
    :return p3d.sys_kinetic(p3d_list): returns the total Ek of the system
    """
    return p3d.sys_kinetic(p3d_list)
# Begin main code

def msd(p3d_list, coord_list_initial, box, part_num):
    """
    Computes the mean square displacement of particles.

    :param p3d_list: list of p3d instances
    :param coord_list_initial: array of initail positions for n particles in the system
    :return msd: the mean square displacement of all the particles
    """
    coord_list = arrayutils.gen_coord_list(p3d_list)
    displacement = arrayutils.disp_calc_msd(arrayutils.mic_norm(coord_list - coord_list_initial , box))
    msd_result = 1/part_num * displacement
    return msd_result

def rdf(mag_array, rho, rdf_file_handle):
    """
    Computes the radial distribution function of the system.

    :param mag_array: array of magnitudes of all r-vectors
    :param rho: float, density specified by user for simulation
    :param rdf_file_handle: file handle for rdf data to be saved to
    :return:
    """
    hist, hist_edge = np.histogram(mag_array, bins=100, weights= 1/(rho*4*np.pi*mag_array**2))
    hist = np.insert(hist, 0, [0])
    hist = np.nan_to_num(hist, nan = 0, posinf=100, neginf=-100) / len(hist)
    np.savetxt(rdf_file_handle, np.vstack((hist_edge, hist)).T, delimiter=',', newline='\n')
    return

def simulate(outfile_name, dt, numstep, number_of_particles, T, rho, pdf_report, animate):
    StartTime = datetime.now()
    #Parse user input for configuration
   
    # Open output files
    outfile = open(outfile_name + '.xyz', "w")
    # Open output file for energy calculations
    outfile_energy = open(outfile_name + '_energy.dat', 'w')
    # Open output file for MSD calculations
    outfile_msd = open(outfile_name + '_msd.dat', 'w')

    #Open output file for kinetic & potential calculations
    outfile_kin = open(outfile_name + '_kin.dat', 'w')
    outfile_pot = open(outfile_name + '_pot.dat', 'w')
    outfile_etot = open(outfile_name + '_etot.dat', 'w')
    # Writes column headings for both files
    outfile_msd.write('#Time, MSD\n')
    outfile_energy.write('#Time, E_Err, MSD\n')
    outfile_kin.write('#Time, E_K\n')
    outfile_pot.write('#Time, E_p\n')
    outfile_etot.write('#Time, E_Tot\n')
    # Initialise parameters 
    outfile_rdf = open(outfile_name + '_rdf.dat', 'w')
    outfile_rdf.write('#Radius, RDF(r) \n')
    time = 0
    m = 1
    x0 = v0 = np.zeros(3)
    # Parse simulation parameters
    dt, numstep, number_of_particles, T, rho = float(dt), int(numstep), int(number_of_particles), float(T), float(rho)
    params = {'time_step': dt, 'num_step': numstep, 'n': number_of_particles, 'temp' : T, 'rho' : rho}
    # Get initial force
    i = 0
    #Initialises particle list
    particle_list = []
    while i < number_of_particles:
        particle_list.append(p3d('particle', m, x0, v0))
        i += 1

    #Initialises box, positions, and velocities of particles

    box, full_lattice = mdutils.set_initial_positions(rho, particle_list)
    mdutils.set_initial_velocities(T, particle_list)
    #Computes pair separation vectors and magnitudes
    r_array = arrayutils.gen_r_array(particle_list, box)
    mag_array = arrayutils.mag_calc(r_array)
    #Computes forces and energies
    force = force_LJ(r_array, mag_array)
    E_p, pot_energy = pot_energy_LJ(mag_array)
    E_k = kinetic(particle_list)
    energy_tot_initial = E_p + E_k
    outfile_kin.write(f'{time}, {E_k}\n')
    outfile_pot.write(f'{time}, {E_p}\n')
    outfile_etot.write(f'{time}, {energy_tot_initial}\n')
    #Stores initial position for use in MSD calculations
    initial_pos = arrayutils.gen_coord_list(particle_list)
    #Initialises np array for RDF calculations
    rdf_mag = np.array([])

    #Begin main integration loop
    for i in range(numstep):
        #Writes initial MSD value
        msd_result = msd(particle_list, initial_pos, box, number_of_particles)
        outfile_msd.write(f'{time}, {msd_result}\n')
        #Update forces
        for particle, f in zip(particle_list, force):
            particle.update_pos_2nd(dt, np.sum(f, axis=0))
            particle.pos = arrayutils.pbc(particle.pos, box)
        
        #Recompute separations and forces
        r_array_new = arrayutils.gen_r_array(particle_list, box)
        mag_array_new = arrayutils.mag_calc(r_array_new)
        force_new = force_LJ(r_array_new, mag_array_new)
        #Update velocities
        for particle, f, f_new in zip(particle_list, force, force_new):
            particle.update_vel(dt, 0.5*np.sum(f, axis = 0) + 0.5*np.sum(f_new, axis = 0))
        #Calculate energy error and write to output
        E_k = kinetic(particle_list)
        E_p = pot_energy_LJ(mag_array_new)[0]
        energy_tot =  E_k + E_p 
        energy_err = (energy_tot - energy_tot_initial)/energy_tot_initial * 100
        outfile_energy.write(f'{time}, {energy_err}\n')
        outfile_kin.write(f'{time}, {E_k}\n')
        outfile_pot.write(f'{time}, {E_p}\n')
        outfile_etot.write(f'{time}, {energy_tot}\n')
        #Store samples for RDF from time = 250 onwards and every 10 time steps.
        if i > 250 and i % 10 == 0:
            np.fill_diagonal(mag_array_new, np.nan)
            mag_values = mag_array_new[~np.isnan(mag_array_new)]
            mag_values_flatten = mag_values.flatten()
            rdf_mag = np.concatenate((rdf_mag, mag_values_flatten))
        # Re-define force value
        force = force_new
        # Increase time
        time += dt
        
        # Output particle information

        # Append information to data lists
        """
        time_list.append(time)
        pos_1_list.append(p1.pos[0])
        pos_2_list.append(p2.pos[0])
        energy_list.append(energy)
        """
        #Writes trajectory file
        num_of_particles = len(particle_list)
        outfile.write(f'{num_of_particles}\n')
        outfile.write(f'Point = {i + 1}\n')
        for i, particle in enumerate(particle_list):
            outfile.write(f's{i + 1} {particle.pos[0]} {particle.pos[1]} {particle.pos[2]}\n')
        
        
    # Post-simulation:
    # Close output file
    outfile.close()
    outfile_energy.close()
    outfile_msd.close()
    print('rdf mag length:', len(rdf_mag))
    rdf(rdf_mag, rho, outfile_rdf)
    outfile_rdf.close()
    executionTime = datetime.now() - StartTime

    # Generates pdf report, if called for
    if pdf_report is True:
        reportutils.genfigs(outfile_name)
        reportutils.genreport(outfile_name, params, executionTime)
    else:
        pass
    if animate is True:
        subprocess.run(['python', 'plot_xyz.py', f'{outfile_name}.xyz', '--3d'])
    else:
        pass
    print('Simulation completed!')
    return 
# Execute main method, but only when directly invoked
if __name__ == "__main__":
    sg.theme('Light Blue 2')
    toggle_btn_off = b'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAED0lEQVRYCe1WTWwbRRR+M/vnv9hO7BjHpElMKSlpqBp6gRNHxAFVcKM3qgohQSqoqhQ45YAILUUVDRxAor2VAweohMSBG5ciodJUSVqa/iikaePEP4nj2Ovdnd1l3qqJksZGXscVPaylt7Oe/d6bb9/svO8BeD8vA14GvAx4GXiiM0DqsXv3xBcJU5IO+RXpLQvs5yzTijBmhurh3cyLorBGBVokQG9qVe0HgwiXLowdy9aKsY3g8PA5xYiQEUrsk93JTtjd1x3siIZBkSWQudUK4nZO1w3QuOWXV+HuP/fL85klAJuMCUX7zPj4MW1zvC0Ej4yMp/w++K2rM9b70sHBYCjo34x9bPelsgp/XJksZ7KFuwZjr3732YcL64ttEDw6cq5bVuCvgy/sje7rT0sI8PtkSHSEIRIKgCQKOAUGM6G4VoGlwiqoVd2Za9Vl8u87bGJqpqBqZOj86eEHGNch+M7otwHJNq4NDexJD+59RiCEQG8qzslFgN8ibpvZNsBifgXmFvJg459tiOYmOElzYvr2bbmkD509e1ylGEZk1Y+Ssfan18n1p7vgqVh9cuiDxJPxKPT3dfGXcN4Tp3dsg/27hUQs0qMGpRMYjLz38dcxS7Dm3nztlUAb38p0d4JnLozPGrbFfBFm79c8hA3H2AxcXSvDz7/+XtZE1kMN23hjV7LTRnKBh9/cZnAj94mOCOD32gi2EUw4FIRUMm6LGhyiik86nO5NBdGRpxYH14bbjYfJteN/OKR7UiFZVg5T27QHYu0RBxoONV9W8KQ7QVp0iXdE8fANUGZa0QAvfhhXlkQcmjJZbt631oIBnwKmacYoEJvwiuFgWncWnXAtuVBBEAoVVXWCaQZzxmYuut68b631KmoVBEHMUUrJjQLXRAQVSxUcmrKVHfjWWjC3XOT1FW5QrWpc5IJdQhDKVzOigEqS5dKHMVplnNOqrmsXqUSkn+YzWaHE9RW1FeXL7SKZXBFUrXW6jIV6YTEvMAUu0W/G3kcxPXP5ylQZs4fa6marcWvvZfJu36kuHjlc/nMSuXz+/ejxgqPFpuQ/xVude9eu39Jxu27OLvBGoMjrUN04zrNMbgVmOBZ96iPdPZmYntH5Ls76KuxL9NyoLA/brav7n382emDfHqeooXyhQmARVhSnAwNNMx5bu3V1+habun5nWdXhwJZ2C5mirTesyUR738sv7g88UQ0rEkTDlp+1wwe8Pf0klegUenYlgyg7bby75jUTITs2rhCAXXQ2vwxz84vlB0tZ0wL4NEcLX/04OrrltG1s8aOrHhk51SaK0us+n/K2xexBxljcsm1n6x/Fuv1PCWGiKOaoQCY1Vb9gWPov50+fdEqd21ge3suAlwEvA14G/ucM/AuppqNllLGPKwAAAABJRU5ErkJggg=='
    toggle_btn_on = b'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABmJLR0QA/wD/AP+gvaeTAAAD+UlEQVRYCe1XzW8bVRCffbvrtbP+2NhOD7GzLm1VoZaPhvwDnKBUKlVyqAQ3/gAkDlWgPeVQEUCtEOIP4AaHSI0CqBWCQyXOdQuRaEFOk3g3IMWO46+tvZ+PeZs6apq4ipON1MNafrvreTPzfvub92bGAOEnZCBkIGQgZOClZoDrh25y5pdjruleEiX+A+rCaQo05bpuvJ/+IHJCSJtwpAHA/e269g8W5RbuzF6o7OVjF8D3Pr4tSSkyjcqfptPDMDKSleW4DKIggIAD5Yf+Oo4DNg6jbUBlvWLUNutAwZu1GnDjzrcXzGcX2AHw/emFUV6Sfk0pqcKpEydkKSo9q3tkz91uF5aWlo1Gs/mYc+i7tz4//19vsW2AU9O381TiioVCQcnlRsWeQhD3bJyH1/MiFLICyBHiuzQsD1arDvypW7DR9nzZmq47q2W95prm+I9fXfqXCX2AF2d+GhI98Y8xVX0lnxvl2UQQg0csb78ag3NjEeD8lXZ7pRTgftmCu4864OGzrq+5ZU0rCa3m+NzXlzvoAoB3+M+SyWQuaHBTEzKMq/3BMbgM+FuFCDBd9kK5XI5PJBKqLSev+POTV29lKB8rT0yMD0WjUSYLZLxzNgZvIHODOHuATP72Vwc6nQ4Uiw8MUeBU4nHS5HA6TYMEl02wPRcZBJuv+ya+UCZOIBaLwfCwQi1Mc4QXhA+PjWRkXyOgC1uIhW5Qd8yG2TK7kSweLcRGKKVnMNExWWBDTQsH9qVmtmzjiThQDs4Qz/OUSGTwcLwIQTLW58i+yOjpXDLqn1tgmDzXzRCk9eDenjo9yhvBmlizrB3V5dDrNTuY0A7opdndStqmaQLPC1WCGfShYRgHdLe32UrV3ntiH9LliuNrsToNlD4kruN8v75eafnSgC6Luo2+B3fGKskilj5muV6pNhk2Qqg5v7lZ51nBZhNBjGrbxfI1+La5t2JCzfD8RF1HTBGJXyDzs1MblONulEqPDVYXgwDIfNx91IUVbAbY837GMur+/k/XZ75UWmJ77ou5mfM1/0x7vP1ls9XQdF2z9uNsPzosXPNFA5m0/EX72TBSiqsWzN8z/GZB08pWq9VeEZ+0bjKb7RTD2i1P4u6r+bwypo5tZUumEcDAmuC3W8ezIqSGfE6g/sTd1W5p5bKjaWubrmWd29Fu9TD0GlYlmTx+8tTJoZeqYe2BZC1/JEU+wQR5TVEUPptJy3Fs+Vkzgf8lemqHumP1AnYoMZSwsVEz6o26i/G9Lgitb+ZmLu/YZtshfn5FZDPBCcJFQRQ+8ih9DctOFvdLIKHH6uUQnq9yhFu0bec7znZ+xpAGmuqef5/wd8hAyEDIQMjAETHwP7nQl2WnYk4yAAAAAElFTkSuQmCC'
    col1 = sg.Column([[
        sg.Frame('Simulation Parameters:', [
            [sg.Text('Integration time step:')],
            [sg.Input(key='-DELTA-T-', size=(10,1))],
            [sg.Text('Integration step number:')],
            [sg.Input(key='-NUM-STEP-', size=(10,1))],
            [sg.Text('Number of atoms:')],
            [sg.Input(key='-NUM-ATOMS-', size=(10,1))],
            [sg.Text('Temperature:')],
            [sg.Input(key='-TEMP-', size=(10,1))],
            [sg.Text('Density:')],
            [sg.Input(key='-RHO-', size=(10,1))],
            [sg.Text('Run Name:')],
            [sg.Input(key='-OUTPUT-NAME-',size=(10,1))],
            [sg.Frame("Load from file:",[
                [sg.Input(key = '-CONFIG-FILE-', size=(15,1)),sg.FileBrowse(key='-CONFIG-FILE-PATH-')],
                [sg.Button('Load')]
            ])],
            [sg.Frame('Example configuration:', [[
                sg.Button('Ar Gas', key='-EX-GAS-'), sg.Button('Ar Solid', key='-EX-SOL-')
            ]])]
        ])
    ]])
    param_keys = [
        '-DELTA-T-',
        '-NUM-STEP-',
        '-NUM-ATOMS-',
        '-TEMP-',
        '-RHO-'
    ]
    ar_gas_config = [
        5E-4,
        20000,
        30,
        1,
        0.05
    ]
    ar_solid_config = [
        0.001,
        10000,
        32,
        0.1,
        1.0
    ]
    results_layout = [
        [sg.Text("Generate PDF report:")],
        [
            sg.Text('Off'),
            sg.Button(image_data=toggle_btn_off, key='-TOGGLE-PDF-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
            sg.Text('On')
        ],
        [sg.Text("Animate:")],
        [
            sg.Text('Off'),
            sg.Button(image_data=toggle_btn_off, key='-TOGGLE-ANIMATE-', button_color=(sg.theme_background_color(), sg.theme_background_color()), border_width=0, metadata=False),
            sg.Text('On')
        ]
    ]
    results_frame = sg.Frame('Results configuration:', results_layout)
    layout = [[col1, sg.Column([[results_frame],[sg.Frame('Output:',[[sg.Multiline(size=(20,8),auto_refresh=True,disabled=True,reroute_stdout=True,autoscroll=True)]])],[sg.Button('Run!', key = '-RUN-'), sg.Button('Cancel')],[sg.Frame('About:',[[sg.Text("Simulation of atoms in a Lennard-Jones potential using PBC and MIC with GUI, by Y. Wang 2022",size=(24,5))]])]])]]
    window = sg.Window('Lennard-Jones Simulator', layout)
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == '-CONFIG-FILE-PATH-':
            window['-CONFIG-FILE-'].update(default_text=window['-CONFIG-FILE-PATH-'].metadata)
        elif event == '-TOGGLE-PDF-':
            window['-TOGGLE-PDF-'].metadata = not window['-TOGGLE-PDF-'].metadata
            window['-TOGGLE-PDF-'].update(image_data = toggle_btn_on if window['-TOGGLE-PDF-'].metadata else toggle_btn_off)
        elif event == '-TOGGLE-ANIMATE-':
            window['-TOGGLE-ANIMATE-'].metadata = not window['-TOGGLE-ANIMATE-'].metadata
            window['-TOGGLE-ANIMATE-'].update(image_data = toggle_btn_on if window['-TOGGLE-ANIMATE-'].metadata else toggle_btn_off)
            print('Animate toggled')
        elif event == '-EX-GAS-':
            for key,val in zip(param_keys, ar_gas_config):
                window[key].update(val)
        elif event == '-EX-SOL-':
            for key,val in zip(param_keys, ar_solid_config):
                window[key].update(val)
        elif event == 'Load':
            try:
                params = np.loadtxt(values['-CONFIG-FILE-'])
            except:
                sg.popup('Import Failed',[[sg.T('Import from file failed.')]])
                break
            for key, param in zip(param_keys, params):
                window[key].update(param)
        elif event == '-RUN-':
            print('Starting Simulation')
            simulate(values['-OUTPUT-NAME-'], values['-DELTA-T-'], values['-NUM-STEP-'],values['-NUM-ATOMS-'], values['-TEMP-'], values['-RHO-'], window['-TOGGLE-PDF-'].metadata,window['-TOGGLE-ANIMATE-'].metadata)
                
    window.close()

