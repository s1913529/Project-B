"""
Report Generation Utility

author: Patrick Wang
Date: 6/3/2022

Generates a pdf report using XeLaTeX / pdfLaTeX for a LJ-simulation run

"""
from datetime import datetime
import os
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
	mpl.use('Agg')

import matplotlib.pyplot as plt
import tikzplotlib
import numpy as np
import subprocess

def genfigs(run_name):
    run_name = str(run_name)
    rdf_data = np.loadtxt(run_name + '_rdf.dat', delimiter = ',', comments = '#')
    msd_data = np.loadtxt(run_name + '_msd.dat', delimiter = ',', comments = '#')
    energy_data = np.loadtxt(run_name + '_energy.dat', delimiter = ',', comments = '#')
    figure = plt.figure()
    plt.plot(rdf_data[0::,0], rdf_data[0::,1])
    plt.xlabel(r'$r$')
    plt.ylabel('RDF')
    plt.title('Radial Distribution Function')
    tikzplotlib.save(run_name+'_rdf.tex')
    plt.clf()

    plt.plot(msd_data[0::,0], msd_data[0::,1])
    plt.xlabel(r'$t$')
    plt.ylabel('MSD')
    plt.title('Mean Square Displacement')
    tikzplotlib.save(run_name+'_msd.tex')
    plt.clf()

    plt.plot(energy_data[0::,0], energy_data[0::,1])
    plt.xlabel(r'$t$')
    plt.ylabel('$\Delta E / E$')
    plt.title('Energy Uncertainty')
    tikzplotlib.save(run_name+'_energy.tex')
    plt.clf()
    
    return 

def genreport(run_name, params, ExecutionTime):
    report = open(run_name+'_report.tex', 'w', encoding='utf-8')
    preamble = [
        '\\documentclass[twocolumn]{article}',
        '\\usepackage[utf8]{inputenc}',
        '\\usepackage{pgfplots}',
        '\\usepackage{booktabs}',
        '\\usepackage{float}',
        '\\usepgfplotslibrary{groupplots,dateplot}',
        '\\usetikzlibrary{patterns,shapes.arrows}',
        '\\pgfplotsset{compat=newest}',
        r'\title{{Lennard-Jones Simulation Report}}',
        r'\author{{Name of Run :\texttt{{{name}}} \\ Time : {time}}}'.format(name = run_name, time = str(datetime.now())),
        '\\date{\\today}', 
        '\\usepackage[top = 2.5cm, bottom=2.5cm, left=2cm, right=2cm]{geometry}',
        '\\usepgfplotlibrary{external}',
        '\\tikzexternalize',
        '\\begin{document}',
        '\\maketitle'
    ]
    delta_t = params['time_step']
    numstep = params['num_step']
    T = params['temp']
    n = params['n']
    rho = params['rho']

    report.writelines("%s\n" % l for l in preamble)
    report.write('\\section{Simulation Parameters}\n')
    report.write(f'The simulation took {ExecutionTime.seconds} seconds to run. \n')
    report.write('The simulation was carried with the following parameters:\n')
    table = [
        '\\begin{table}[H]' ,
        '\\centering'
        '\\begin{tabular}{@{}ccc@{}}',
        '\\toprule',
        'Parameter  & Name                 & Value \\\\ \\midrule',
        f'$\\delta t$ & Time Step            & {delta_t}  \\\\',
        f'$N$        & Number of steps      & {numstep}  \\\\',
        f'$T$        & Temperature          & {T} \\\\',
        f'$n$        & Number of Particles  & {n}  \\\\',
        f'$\\rho$     & Density of Particles & {rho}  \\\\ \\bottomrule',
        '\end{tabular}',
        '\end{table}'
    ]
    report.writelines("%s\n" % l for l in table)

    report.write('\\section{Generated Graphs}')
    msd_file = run_name + '_msd.tex'
    rdf_file = run_name + '_rdf.tex'
    energy_file = run_name + '_energy.tex'
    plots = [
        r'\begin{figure}[H]',
        r'\input{{{energy_file}}}'.format(energy_file = energy_file),
        r'\end{figure}',
        r'\begin{figure}[H]',
        r'\input{{{rdf_file}}}'.format(rdf_file = rdf_file),
        r'\end{figure}',
        r'\begin{figure}[H]',
        r'\input{{{msd_file}}}'.format(msd_file = msd_file),
        r'\end{figure}'
    ]
    report.writelines("%s\n" % l for l in plots)
    report.write('\\end{document}')
    report.close()
    report_file = run_name + '_report.tex'
    command = f'pdflatex {report_file}'
    print(command)

    # compile TeX file
    subprocess.run(['lualatex', '-interaction=nonstopmode', '--shell-escape', report_file])

    return
