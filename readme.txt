CompMod Lennard-Jones Simulation

Author: Patrick Wang 
s1913529@ed.ac.uk
Date: 12.02.2022

====================
Table of Contents
1. LJ_simulation.py
2. particle_3D.py
3. mdutilities_20210131.py
4. arrayutils.py
5. plot_xyz.py
====================

1. LJ_simulation.py

The main python script that contains method to calculate forces, potential, and
velocity Verlet time integrator. 

2. particle_3D.py

Imported as a package in LJ_simulation.py to provide the particle_3D class.

3. mdutilities_20210131.py

Provided with methods to set the initial velocities and position in a fcc crystal given
simulation parameters.

4. arrayutils.py

A package called in LJ_simulation.py to compute pair separation using numpy.

5. plot_xyz.py

Provided script to animate .xyz trajectory files.

6. reportutils.py

A package called in LJ_simulation.py to generate a pdf report of the simulation run. To use, add --report flag when calling LJ_simulation.py. To use, pdfLaTeX must be installed and on PATH.
============
Usage
============

To use, run the following command:
python3 LJ_simulation.py <input_file> <name_of_simulation_run>

#Options#

--report : generates report with information of simulation run using pdfLaTeX

where the input_file contains the simulation parameters, and an example
one was supplied for the two conditions given by the exercise instruction: as
Ar_solid_config.txt and Ar_gas_config.txt

Different observables will be outputted to different files as generated:
Trajectory => name_of_simulation_run.xyz
Energy Error => name_of_simulation_run_energy.dat
Mean Square Displacement => name_of_simulation_run_msd.dat
Radial Distribution Function => name_of_simulation_run_rdf.dat

This allows for subsequent analysis with numpy and matplotlib.

Data from two runs with given parameters are included. 
