"""
Computer Modelling: A Lennard-Jones simulation of N-particles 
with Verlet Time Integrator

This programme uses the Verlet integration
method to simulate the dynamics of particles interacting
with a Lennard-Jones potential in a fcc. 

author: P. Wang s1913529
"""

import sys
import numpy as np
import matplotlib.pyplot as pyplot
from pandas import array
from particle3D import Particle3D as p3d
import scipy.constants as constants
from scipy import constants
from scipy.signal import find_peaks
import arrayutils
import mdutilities_20210131 as mdutils

def force_LJ(r_array, mag_array):
    i = 0
    force = []
    """
    while i < len(r_array):
        F = 48*(np.power(mag_array[i],-14.*np.ones_like(mag_array[i])) - 0.5*np.power(mag_array[i], -8.*np.ones_like(mag_array[i])))*r_array[i]
        F = np.nan_to_num(F, nan = 0, posinf = 0, neginf = 0)
        force.append(F)
        i += 1
    """
    for row, mag in zip(r_array, mag_array):
        mag = np.reshape(mag, (r_array.shape[0],1))
        F = 48*(np.power(mag, -14.*np.ones_like(mag)) - 0.5*np.power(mag, -8.*np.ones_like(mag)))*row
        F = np.nan_to_num(F, nan = 0, posinf = 0, neginf = 0)
        force.append(F)
    return np.array(force)


def pot_energy_LJ(mag_array):
    potential = 4*(np.power(mag_array, -12.*np.ones_like(mag_array))- np.power(mag_array, -6.*np.ones_like(mag_array)))
    potential = np.nan_to_num(potential, nan=0, posinf=0, neginf=0)
    E_p_total = 0.5*np.sum(potential)
    return E_p_total, potential

def kinetic(p3d_list):
    return p3d.sys_kinetic(p3d_list)
# Begin main code



def main():
    if len(sys.argv)!=3:
        print("Wrong number of arguments.")
        print("Usage: " + sys.argv[0] + " <input file>" + "<output file>")
        quit()
    else:
        infile_name = sys.argv[1]
        outfile_name = sys.argv[2]
    # Open output file
    outfile = open(outfile_name, "w")
    """
    time = 0
    m = 1
    x0 = v0 = np.zeros(3)
    dt, numstep, number_of_particles, T, rho = np.loadtxt(infile_name, comments='#', unpack=True)
    dt, numstep, number_of_particles, T, rho = float(dt), int(numstep), int(number_of_particles), float(T), float(rho)


    # Get initial force
    i = 0
    particle_list = []
    while i < number_of_particles:
        particle_list.append(p3d('particle', m, x0, v0))
        i += 1

    box, full_lattice = mdutils.set_initial_positions(rho, particle_list)
    mdutils.set_initial_velocities(T, particle_list)
    r_array = arrayutils.gen_r_array(particle_list, box)
    mag_array = arrayutils.mag_calc(r_array)
    force = force_LJ(r_array, mag_array)
    E_p, pot_energy = pot_energy_LJ(mag_array)
    E_k = kinetic(particle_list)
    """
    r1 = np.array([0,0,0])
    r2 = np.array([2**(1/6)+0.1, 0, 0])
    v1 = np.array([0.01, 0,0])
    v2 = np.array([-0.01, 0, 0])
    m = 1
    box = np.array([3,3,3])
    dt = 0.01
    time = 0
    numstep = 1000
    particle_list = [p3d('particle1', m, r1, v1),
                    p3d('particle2', m, r2, v2)
                    ]
    r_array = arrayutils.gen_r_array(particle_list, box)
    mag_array = arrayutils.mag_calc(r_array)
    force = force_LJ(r_array, mag_array)
    E_p, pot_energy = pot_energy_LJ(mag_array)
    E_k = kinetic(particle_list)
    energy_tot_initial = E_p + E_k
    for i in range(numstep):
        for particle, f in zip(particle_list, force):
            #print(particle.pos.shape)
            #print('f shape:', np.sum(f,axis = 0).shape)
            #print('v shape:', particle.vel.shape)
           # print('mass:', particle.mass)
            particle.update_pos_2nd(dt, np.sum(f, axis=0))
            #print(particle.pos.shape)
            particle.pos = arrayutils.pbc(particle.pos, box)
           # print(particle.pos.shape)
        
        #print(f'{i}th simulation, coord_list length: {arrayutils.gen_coord_list(particle_list).shape}')
        r_array_new = arrayutils.gen_r_array(particle_list, box)
        #print(f'{i}th simulation,particle_list length: {len(particle_list)}')
        mag_array_new = arrayutils.mag_calc(r_array_new)
        force_new = force_LJ(r_array_new, mag_array_new)
        
        
         
        # Update particle velocity by averaging
        # current and new forces
        for particle, f, f_new in zip(particle_list, force, force_new):
            particle.update_vel(dt, 0.5*np.sum(f, axis = 0) + 0.5*np.sum(f_new, axis = 0))
        energy_tot = kinetic(particle_list) + pot_energy_LJ(mag_array_new)[0]
        energy_err = (energy_tot - energy_tot_initial)/energy_tot_initial * 100
        outfile.write(f'{time}, {particle_list[0].pos[0]}, {particle_list[1].pos[0]}, {energy_err}\n')
        # Re-define force value
        force = force_new
        # Increase time
        time += dt
        
        # Output particle information

        # Append information to data lists
        """
        time_list.append(time)
        pos_1_list.append(p1.pos[0])
        pos_2_list.append(p2.pos[0])
        energy_list.append(energy)
        """
        """
        num_of_particles = len(particle_list)
        outfile.write(f'{num_of_particles}\n')
        outfile.write(f'Point = {i + 1}\n')
        for i, particle in enumerate(particle_list):
            outfile.write(f's{i + 1} {particle.pos[0]} {particle.pos[1]} {particle.pos[2]}\n')
        """
        
    # Post-simulation:
    # Close output file
    outfile.close()
    """
    delta_E = np.max(energy_list) - np.min(energy_list)
    E_err = delta_E / energy_list[0]
    # Computes the period and energy uncertainty
    nu_bar = period_calc(outfile_name)
    print('Uncertainty in energy for this simulation run is {}'.format(E_err))
    with open(outfile_name + 'REPORT', 'w') as f:
        f.write("#### Simulation Parameters ####\n")
        f.write('D_e = {}, r_e = {}, alpha = {}, dt = {}, numstep = {}\n'.format(D_e, r_e, alpha, dt, numstep))
        f.write('#### Simulation Result ####\n')
        f.write('E_err = {}, nu_bar = {}'.format(E_err, nu_bar))
    # Plot particle trajectory to screen
    pyplot.title('Velocity Verlet: position vs time')
    pyplot.xlabel('Time')
    pyplot.ylabel('Position')
    pyplot.plot(time_list, pos_1_list, label = 'Particle 1')
    pyplot.plot(time_list, pos_2_list, label = 'Particle 2')
    pyplot.legend()
    pyplot.show()

    # Plot particle energy to screen
    pyplot.title('Velocity Verlet: total energy vs time')
    pyplot.xlabel('Time')
    pyplot.ylabel('Energy')
    pyplot.plot(time_list, energy_list)
    pyplot.show()
    """

# Execute main method, but only when directly invoked
if __name__ == "__main__":
    main()

