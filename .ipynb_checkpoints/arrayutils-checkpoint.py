import numpy as np

def pbc(x1, box):
    return np.mod(x1, box)

def mic(x1, box):
    box = np.tile(box, (x1.shape[0], x1.shape[0], 1))
    return np.mod(x1 + 0.5*box, box) - 0.5*box

def gen_coord_list(p3d_list):
    print(p3d_list[0].pos)
    coord_list = []
    for particle in p3d_list:
        coord_list.append(particle.pos)
    return np.array(coord_list)

def gen_r_array(p3d_list, box):
    print(f'len(p3d_list) = {len(p3d_list)}')
    pos_list = gen_coord_list(p3d_list)
    print(f'len(pos_list)={pos_list.shape}')
    pos_row = pos_list.reshape((1, 3*len(p3d_list)))
    r_array = np.tile(pos_list, len(p3d_list)) - pos_row
    return mic(r_array.reshape((len(p3d_list),len(p3d_list),3)), box)

def mag_calc(r_array):
    mag_array = np.sqrt(np.add.reduce(r_array**2,2))
    return mag_array