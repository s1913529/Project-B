import os
import numpy as np

epsilon = np.linspace(1, 2, 11)

rho = 0.85

for epsilon_value in epsilon:
    with open(f'meltingsim_{epsilon_value}.txt', 'w') as f:
        f.write(f'0.0005 \n 20000 \n 30 \n {epsilon_value} \n {rho}')

for epsilon_value in epsilon:
    os.system(f'nohup python3 LJ_simulation.py meltingsim_{epsilon_value}.txt meltsim_{epsilon_value} --report &')