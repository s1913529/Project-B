"""
 CompMod Ex2: Particle3D, a class to describe point particles in 3D space

 An instance describes a particle in Euclidean 3D space: 
 velocity and position are [3] arrays


author: Patrick Wang s1913529

"""
import numpy as np

class Particle3D(object):
    """
    Class to describe point-particles in 3D space

        Properties:
    label: name of the particle
    mass: mass of the particle
    pos: position of the particle
    vel: velocity of the particle

        Methods:
    __init__
    __str__
    kinetic_e  - computes the kinetic energy
    momentum - computes the linear momentum
    update_pos - updates the position to 1st order
    update_pos_2nd - updates the position to 2nd order
    update_vel - updates the velocity

        Static Methods:
    new_p3d - initializes a P3D instance from a file handle
    sys_kinetic - computes total K.E. of a p3d list
    com_velocity - computes total mass and CoM velocity of a p3d list
    """

    def __init__(self, label, mass, position, velocity):
        """
        Initialises a particle in 3D space

        :param label: String w/ the name of the particle
        :param mass: float, mass of the particle
        :param position: [3] float array w/ position
        :param velocity: [3] float array w/ velocity
        """
        self.label = label
        self.mass = mass
        self.pos = position
        self.vel = velocity


    def __str__(self):
        """
        XYZ-compliant string. The format is
        <label>    <x>  <y>  <z>
        """
        return "{}    {}   {}   {}\n".format(self.label, self.pos[0], self.pos[1], self.pos[2])


    def kinetic_e(self):
        """
        Returns the kinetic energy of a Particle3D instance

        :return ke: float, 1/2 m v**2
        """
        return 0.5 * self.mass * np.sum(self.vel**2)


    def momentum(self):
        """
        Returns the linear momentum of a Paticle3D instance
        """
        return self.mass * self.vel


    def update_pos(self, dt):
        """
        Updates the position of a Particle3D instance to the first order

        :param dt: A small change in time
        """
        self.pos = self.pos + dt * self.vel


    def update_pos_2nd(self, dt, force):
        """
        Updates the position of a Particle3D instance to the second order.

        :param dt: A small change in time
        :param force: [3] float array with x,y,z components of force
        """
        self.pos = self.pos + dt * self.vel + dt**2 * force / (2* self.mass)


    def update_vel(self, dt, force):
        """
        Updates the velocity of a Particle3D instance with a supplied force.

        :param dt: A small change in time
        :param force: [3] float array with x,y,z components of force
        """
        self.vel = self.vel + dt * force / self.mass

    @staticmethod
    def new_particle(file_handle):
        """
        Initialises a Particle3D instance given an input file handle.
        np.array(force)
        The input file should contain one line per planet in the following format:
        label   <mass>  <x> <y> <z>    <vx> <vy> <vz>
        
        :param inputFile: Readable file handle in the above format

        :return Particle3D instance
        """
        line = file_handle.readline()
        if not line.startswith('#'):

            data = line.split()
            label = str(data[0])
            mass = float(data[1])
            position = np.array([float(data[2]), float(data[3]), float(data[4])])
            velocity = np.array([float(data[5]), float(data[6]), float(data[7])])
        
            return Particle3D(label, mass, position, velocity)
        elif line.startswith('#'):
            raise Exception('Did you prune the comment line?')

    @staticmethod
    def sys_kinetic(p3d_list):
        """
        Gives the kinetic energy of a system of P3D instances, provided as a list.

        :param p3d_list: list in which each item is a P3D instance
        :return sys_ke: The total kinetic energy of the system
        """
        i = 0
        sys_ke = 0
        for i in range(0, len(p3d_list)):
            sys_ke = sys_ke + p3d_list[i].kinetic_e()
            i += 1
        return sys_ke

    @staticmethod
    def com_velocity(p3d_list):
        """
        Computes the total mass and CoM velocity of a list of P3D's

        :param p3d_list: list in which each item is a P3D instance
        :return total_mass: The total mass of the system 
        :return com_vel: Centre-of-mass velocity
        """
        i = 0
        vel_sum = 0
        total_mass = 0
        for i in range(0, len(p3d_list)):
            total_mass += p3d_list[i].mass
            vel_sum += (p3d_list[i].mass * p3d_list[i].vel)
            i += 1
        return total_mass, vel_sum / total_mass


